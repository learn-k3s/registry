# registry

## Create unsecure registry

- remark: I defined the parameters of my registry here: `registry.config`
  - `registry_name="registry"`
  - `registry_domain="registry.dev.test"`
- run this command to greate the registry: `./create.docker.registry.sh`

Add this to your `hosts` file: `192.168.64.20 registry.dev.test` (`192.168.64.20` is the IP of the VM, you can obtain it with this command `echo $(multipass info ${registry_name} | grep IPv4 | awk '{print $2}')
`)

Then, you need to add (or update) this to the docker client settings to allow the connection to the unsecure registry:

```json
{
  "insecure-registries": [
  "registry.dev.test:5000"
  ]
}
```

## Allow K3S to connect to the external unsecure registry

### registries.yaml

> Ref: [https://rancher.com/docs/k3s/latest/en/installation/private-registry/](https://rancher.com/docs/k3s/latest/en/installation/private-registry/)

On every node of the cluster update or **create** `/etc/rancher/k3s/registries.yaml` with this content:

```json
mirrors:
  "registry.dev.test:5000":
    endpoint:
      - "http://registry.dev.test:5000"
```

> - to access to the shell of a node: `multipass shell name_of_the_node`

### /etc/hosts

With **multipass** the system is configured to manage the `hosts` file. So if you update manually the `/etc/hosts` file, the changes will disappears at the next reboot. Instead, you must update (**on every node of the cluster**) this file `/etc/cloud/templates/hosts.debian.tmpl` and add this entry `192.168.64.20 registry.dev.test` (don't forget to use the appropriate IP)

✋✋✋ **and now, restart your cluster**


    hosts registry.dev.test.hosts {
      hosts {
          192.168.64.20 registry.dev.test
          fallthrough
      }
    }




## Add image(s) to the registry

```bash
docker pull node:12.0-slim
docker tag node:12.0-slim registry.dev.test:5000/node:12.0-slim
docker push registry.dev.test:5000/node:12.0-slim
```

> `registry.dev.test` is the domain of my registry (change it if needed)

### Checks

```bash
curl http://registry.dev.test:5000/v2/_catalog
# {"repositories":["node"]}
```

```bash
curl registry.dev.test:5000/v2/node/tags/list
# {"name":"node","tags":["12.0-slim"]}
```

## Stop/Start the registry

### Stop 

```bash
eval $(cat registry.config)
multipass stop ${registry_name}
"
```

### Start

```bash
eval $(cat registry.config)
multipass --verbose exec ${registry_name} -- sudo -- sh -c "
  usermod -a -G docker ubuntu
  chmod 666 /var/run/docker.sock
  docker start registry
"
```
