#!/bin/sh
eval $(cat registry.config)

multipass launch --name ${registry_name} --cpus 1 --mem 1G

IP=$(multipass info ${registry_name} | grep IPv4 | awk '{print $2}')

echo "👋 Initialize 🐳 ${registry_name}..."

multipass --verbose exec ${registry_name} -- sudo -- sh -c "
  apt-get update
  apt-get install -y docker.io
  docker run -d -p 5000:5000 --restart=always --name registry registry:2
"

echo "🖐 add this to \`hosts\` file(s):"
echo "${IP} ${registry_domain}"

echo "🖐 add this to your 🐳 client (host side)"
echo '{'
echo '  "insecure-registries": ['
echo "  \"${registry_domain}:5000\""
echo '  ]'
echo '}'

echo "🖐 update \`/etc/rancher/k3s/registries.yaml\` on every node:"

echo 'mirrors:'
echo '  "registry.dev.test:5000":'
echo '    endpoint:'
echo '      - "http://registry.dev.test:5000"'


# run this after every start of the registry
multipass --verbose exec ${registry_name} -- sudo -- sh -c "
  usermod -a -G docker ubuntu
  chmod 666 /var/run/docker.sock
  docker start registry
"